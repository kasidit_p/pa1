package stopwatch;
/**
 * this class runs a task using a Stopwatch and prints the elapsed time.
 * @author Kasidit Phoncharoen
 * @version 1.0
 */
public class TaskTimer {
	/**
	 * runs a task using a Stopwatch and prints the elapsed time.
	 * @param runnable for each task
	 */
	public static void measureAndPrint(Runnable runnable){
		StopWatch timer = new StopWatch();
		timer.start();
		runnable.run();
		timer.stop();
		double elapsed = timer.getElapsed(); 
		System.out.println(runnable.toString());
		System.out.printf("Elapsed time %.6f sec\n\n", elapsed);
	}
}
