package stopwatch;
/**
 * behavior for each task.
 * @author Kasidit Phoncharoen
 *
 */
public interface Runnable {
	/** each task have diffence work to do. */
	public void run();
}
