package stopwatch;
/**
 * task 4: add Double objects from an array.
 * @author Kasidit Phoncharoen
 * @version 1.0
 */
public class Task4 implements Runnable{
	/** total round. */
	private int counter;
	/** array size. */
	static final int ARRAY_SIZE = 500000;
	/** double array. */
	private Double[] values;
	/** sum of values. */
	private Double sum;
	/**
	 * constructor of task4.
	 * @param couter 
	 */
	public Task4(int couter){
		this.counter = couter;
		values = new Double[ARRAY_SIZE];
		sum = new Double(0.0);
	}
	/**
	 * add Double objects from an array.
	 */
	public void run() {
		System.out.printf("Sum array of Double objects with count=%,d\n", counter);
		
		for(int i=0; i<ARRAY_SIZE; i++) 
			values[i] = new Double(i+1);
		
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum + values[i];
		}
		
	}
	/**
	 * return string of sum.
	 * @return sum
	 */
	public String toString(){
		return "sum = " + sum;
	}
}
