package stopwatch;
/**
 * task 2: append chars to a StringBuilder.
 * @author Kasidit Phoncharoen
 * @version 1.0
 */
public class Task2 implements Runnable{
	/** total round. */
	private int counter;
	/** char that use to sum.*/
	final char CHAR = 'a';
	/** contain char.*/
	private StringBuilder builder;
	/** result of adding char. */
	private String result;
	/**
	 * constructor for task2.
	 * @param counter 
	 */
	public Task2(int counter){
		this.counter = counter;
		builder = new StringBuilder();
	}
	/** add char to StringBuilder in counter round. */
	public void run() {
		System.out.printf("Append to StringBuilder with count=%,d\n", counter);
		int k = 0;
		while(k++ < counter) {
			builder = builder.append(CHAR);
		}
		result = builder.toString();
	}
	/**
	 * return string of final string length.
	 * @return string length
	 */
	public String toString(){
		return "final string length = " + result.length();
	}

}
