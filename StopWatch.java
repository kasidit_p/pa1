package stopwatch;

/**
 * A StopWatch that measures elapsed time between a starting time
 * and stopping time, or until the present time.
 * @author Kasidit Phoncharoen
 * @version 1.0
 */
public class StopWatch {
	/**  time that the stopwatch was started, in nanoseconds. */
	private long startTime;
	/**  time that the stopwatch was stoped, in nanoseconds. */
	private long stopTime;
	/** constant for converting nanoseconds to seconds. */
	private static final double NANOSECONDS = Math.pow(10,9);
	/** constructor of stopwatch class.*/
	public StopWatch() {
		startTime = 0;
		stopTime = 0;
	}
	/** 
	 * method that get elapsed time from start until now or from start to stop time.
	 * @return time from start until now if time is stilling running
	 * 		   start time to stop time if time is already stop 
	 * */
	public double getElapsed() {
		if(isRunning()){
			return (System.nanoTime() - startTime)/(NANOSECONDS);
		}
		else{
			return (stopTime - startTime)/(NANOSECONDS);
		}
	}
	/**
	 * check if time is running or not.
	 * @return true if time is stilling running
	 * 		   false if time is already stop
	 */
	public boolean isRunning(){
		if(stopTime == 0)
			return true;
		return false;
	}
	/** start the stopwatch.*/
	public void start(){
		startTime = System.nanoTime();
	}
	/** stop the stopwatch. */
	public void stop(){
		stopTime = System.nanoTime();
	}
}
