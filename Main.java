package stopwatch;
/**
 * main class for testing each task.
 * @author Kasidit Phoncharoen
 * @version 1.0
 */
public class Main {
	/** creating the specific tasks we want to measure and calling TaskTimer.
	 * @param args 
	 * */
	public static void main(String[]args){
		Runnable[] task = {new Task1(100000),new Task2(100000),new Task3(100000000),new Task4(100000000),new Task5(100000000)};
		TaskTimer.measureAndPrint(task[0]);
		TaskTimer.measureAndPrint(task[1]);
		TaskTimer.measureAndPrint(task[2]);
		TaskTimer.measureAndPrint(task[3]);
		TaskTimer.measureAndPrint(task[4]);
	}
}
