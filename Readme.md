# Stopwatch by Kasidit Phoncharoen (5710546151) 
I ran the tasks on a Macbook air 2012 , and got these. 
results:

Task                                  | Time 
--------------------------------------|-------
Append to String with count=100,000       | 3.543463 sec
Append to StringBuilder with count=100,000 | 0.006628 sec
Sum array of double primitives with count=100,000,000 | 0.208008 sec
Sum array of Double objects with count=100,000,000 | 0.696275 sec
Sum array of BigDecimal with count=100,000,000 | 1.349863 sec

## Explanation of Result
The big difference in time used to to append chars to a String and to a StringBuilder is in String have to use memory more than StringBuilder. In String it is immutable, so once you create string it create memory reference each time. But in StringBuilder, it is mutable( it can change). So it use less memory than String.

The double is the fatest because Double tpye the value is store inside an object which needs llocation, deallocation, memory management plus getters and setters. This take more time than double. Bigdecimal is the slowest because it is more presion than double which mean more memory that were used.