package stopwatch;
/**
 * task 3: add double primitives from an array.
 * @author Kasidit Phoncharoen
 * @version 1.0
 */
public class Task3 implements Runnable{
	/** size of array. */
	static final int ARRAY_SIZE = 500000;
	/** total round. */
	private int counter;
	/** sum of array. */
	private double sum;
	/** value of double primitives. */
	private double[] values;
	/**
	 * constructor of task3.
	 * @param counter 
	 */
	public Task3(int counter){
		this.counter = counter;
		sum = 0;
		values = new double[ARRAY_SIZE];
	}
	/**
	 * add double primitives from an array.
	 */
	public void run() {
		System.out.printf("Sum array of double primitives with count=%,d\n", counter);
		
		for(int k=0; k<ARRAY_SIZE; k++) 
			values[k] = k+1;
		
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum + values[i];
		}
	}
	/**
	 * string of sum.
	 * @return sum
	 */
	public String toString(){
		return "sum = " + sum; 
	}
}
