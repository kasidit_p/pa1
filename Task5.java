package stopwatch;

import java.math.BigDecimal;
/**
 * task 5: add BigDecimal objects from an array.
 * @author Kasidit Phoncharoen
 * @version 1.0
 */
public class Task5 implements Runnable{
	/** total round. */
	int counter;
	/** array size. */
	static final int ARRAY_SIZE = 500000;
	/** array of BigDecimal. */
	BigDecimal[] values;
	/** sum of values. */
	BigDecimal sum ;
	/**
	 * constructor of task5.
	 * @param counter 
	 */
	public Task5(int counter){
		this.counter = counter;
		values = new BigDecimal[ARRAY_SIZE];
		sum = new BigDecimal(0.0);
	}
	/**
	 * add BigDecimal objects from an array.
	 */
	public void run() {
		System.out.printf("Sum array of BigDecimal with count=%,d\n", counter);
		for(int i=0; i<ARRAY_SIZE; i++) 
			values[i] = new BigDecimal(i+1);
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum.add( values[i] );
		}
	}
	/**
	 * return string sum.
	 * @return sum
	 */
	public String toString(){
		return "sum = " + sum;
	}

}
