package stopwatch;
/**
 * task that append chars to a string.
 * @author Kasidit Phonchareon
 *
 */
public class Task1 implements Runnable{
	/** total round. */
	private int counter;
	/** sum of char. */
	private String sum ;
	/** char that use to add. */
	final char CHAR = 'a';
	/** 
	 * constructor for task1.
	 * @param counter 
	 * */
	public Task1(int counter){
		this.counter = counter;
		sum = "";
	}
	/** add char to String in counter round. */
	public void run() {
		System.out.printf("Append to String with count=%,d\n", counter);
		int k = 0;
		while(k++ < counter) {
			sum = sum + CHAR;
		}
	}
	/** 
	 * string of final string length.
	 * @return length of sum
	 * */
	public String toString(){
		return ("final string length = " + sum.length());
	}
}
